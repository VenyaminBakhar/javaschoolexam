package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (!isStatementCorrect(statement)) {
            return null;
        }
        try {
            LinkedList<Double> valueStack = new LinkedList<>();
            LinkedList<Character> operatorStack = new LinkedList<>();

            for (int i = 0; i < statement.length(); i++) {
                char currSym = statement.charAt(i);

                if (currSym == '(') {
                    operatorStack.add('(');
                } else if (currSym == ')') {
                    while (operatorStack.getLast() != '(') {
                        calculateOperation(valueStack, operatorStack.removeLast());
                    }
                    operatorStack.removeLast();
                } else if (isOperator(currSym)) {
                    while (!operatorStack.isEmpty() &&
                            getPriority(operatorStack.getLast()) >= getPriority(currSym)) {

                        calculateOperation(valueStack, operatorStack.removeLast());
                    }
                    operatorStack.add(currSym);
                } else {
                    String value = "";
                    while (i < statement.length() &&
                            (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {

                        value += statement.charAt(i++);
                    }
                    --i;
                    valueStack.add(Double.parseDouble(value));
                }
            }

            while (!operatorStack.isEmpty()) {
                calculateOperation(valueStack, operatorStack.removeLast());
            }

            return formatDoubleResult(valueStack.get(0));
        } catch (NoSuchElementException | ArithmeticException | NumberFormatException ex) {
            return null;
        }
    }

    private boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private int getPriority(char operator) {
        if (operator == '*' || operator == '/') {
            return 1;
        }

        if (operator == '+' || operator == '-') {
            return 0;
        }

        return -1;
    }

    private void calculateOperation(LinkedList<Double> valueStack, char operator) {
        double valueOne = valueStack.removeLast();
        double valueTwo = valueStack.removeLast();

        switch (operator) {
            case '+':
                valueStack.add(valueTwo + valueOne);
                break;
            case '-':
                valueStack.add(valueTwo - valueOne);
                break;
            case '*':
                valueStack.add(valueTwo * valueOne);
                break;
            case '/':
                valueStack.add(valueTwo / valueOne);
                break;
            default:
                throw new IllegalArgumentException("Unknown operator: " + operator);
        }
    }

    private boolean isStatementCorrect(String statement) {
        if (statement == null || statement.isEmpty()) {
            return false;
        }

        int bracketCounter = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                bracketCounter++;
            }
            if (statement.charAt(i) == ')') {
                bracketCounter--;
            }

            if (statement.charAt(i) == ',' || bracketCounter < 0) {
                return false;
            }
        }
        return true;
    }

    private String formatDoubleResult(double d) {
        if (Double.isInfinite(d)) {
            return null;
        }

        if (d == (long) d) {
            return String.valueOf((long) d);
        } else {
            return String.valueOf(d);
        }
    }
}

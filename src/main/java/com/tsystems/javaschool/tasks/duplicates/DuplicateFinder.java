package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException();
        }

        List<String> inputLines;
        try {
            inputLines = Files.readAllLines(sourceFile.toPath());
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        Collections.sort(inputLines);

        List<String> results = new ArrayList<>();
        String previousLine = inputLines.get(0);
        int repeatCounter = 0;
        for (String line : inputLines) {
            if (previousLine.equals(line)) {
                repeatCounter++;
            } else {
                results.add(previousLine + "[" + repeatCounter + "]");
                previousLine = line;
                repeatCounter = 1;
            }
        }
        results.add(previousLine + "[" + repeatCounter + "]");

        try {
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(targetFile, true)));
            for (String line : results) {
                writer.println(line);
            }
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

}


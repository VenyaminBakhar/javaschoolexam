package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        int elementCounter = 0;
        int indexOfPrevElem = -1;
        for (int i = 0; i < x.size(); i++) {
            int indexOfCurrElem = y.indexOf(x.get(i));

            if (indexOfCurrElem == -1) {
                return false;
            }
            if (indexOfPrevElem < indexOfCurrElem) {
                elementCounter++;
                indexOfPrevElem = indexOfCurrElem;
            }
        }

        if (elementCounter == x.size()) {
            return true;
        } else {
            return false;
        }
    }
}
